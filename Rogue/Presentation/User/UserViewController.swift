import UIKit

class UserViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.dataSource = self
    tableView.delegate = self
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "section", for: indexPath) as! UserSectionCell
    cell.nameLabel.font = UIFont.preferredFont(forTextStyle: .title2, withSymbolicTraits: .traitBold)
    
    switch indexPath.row {
    case 0:
      cell.nameLabel.text = "Recently Played"
      cell.descriptionLabel.text = nil
    case 1:
      cell.nameLabel.text = "Loans"
      cell.descriptionLabel.text = nil
    case 2:
      cell.nameLabel.text = "Holds"
      cell.descriptionLabel.text = nil
    case 3:
      cell.nameLabel.text = "Wishlist"
      cell.descriptionLabel.text = nil
    case 4:
      cell.nameLabel.text = "Activity"
      cell.descriptionLabel.text = nil
    default:
      cell.nameLabel.text = nil
      cell.descriptionLabel.text = nil
    }
    
    if indexPath.row % 2 == 0 {
      cell.contentView.backgroundColor = UIColor(displayP3Red: 240 / 255, green: 240 / 255, blue: 240 / 255, alpha: 1)
    }
    else {
      cell.contentView.backgroundColor = UIColor(displayP3Red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1)
    }

    return cell
  }
}
