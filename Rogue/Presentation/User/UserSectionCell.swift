import UIKit

class UserSectionCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var collectionView: UICollectionView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.dataSource = self
    collectionView.delegate = self
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 10
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! UserItemCell
    cell.topChromeView.backgroundColor = .white
    cell.topChromeView.layer.cornerRadius = 8
    cell.topChromeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    cell.bottomChromeView.backgroundColor = UIColor(displayP3Red: 76 / 255, green: 2 / 255, blue: 3 / 255, alpha: 1)
    cell.bottomChromeView.layer.cornerRadius = 8
    cell.bottomChromeView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    cell.artworkImageView.image = UIImage(named: "Artwork1-1")
    
    cell.itemTextLabel.font = UIFont.preferredFont(forTextStyle: .caption1, withSymbolicTraits: .traitBold)
    cell.itemDetailTextLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
    
    cell.itemTextLabel.text = "Harry Potter and the Sorcerer's Stone"
    cell.itemDetailTextLabel.text = "J.K. Rowling"
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 264, height: 153)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 20
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 20
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
  }
}
