import UIKit

class UserItemCell: UICollectionViewCell {
  @IBOutlet var artworkImageView: UIImageView!
  @IBOutlet var bottomChromeView: UIView!
  @IBOutlet var itemDetailTextLabel: UILabel!
  @IBOutlet var itemTextLabel: UILabel!
  @IBOutlet var topChromeView: UIView!
}
