import Foundation
import UIKit

extension UIFont {
  static func preferredFont(forTextStyle textStyle: UIFont.TextStyle, withSymbolicTraits symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont {
    let descriptor = UIFont.preferredFont(forTextStyle: textStyle).fontDescriptor.withSymbolicTraits(symbolicTraits)
    return UIFont(descriptor: descriptor!, size: 0)
  }
}

