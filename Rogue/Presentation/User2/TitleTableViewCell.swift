import UIKit

class TitleTableViewCell: UITableViewCell {
  @IBOutlet var productListingArtworkContainerView: UIView!
  @IBOutlet var productListingArtworkImageView: UIImageView!
  @IBOutlet var borderView: UIView!
  @IBOutlet var productListingAuthorLabel: UILabel!
  @IBOutlet var productListingBottomBackgroundView: UIView!
  @IBOutlet var productListingTitleLabel: UILabel!
  @IBOutlet var productListingTopBackgroundView: UIView!
  @IBOutlet var sectionBodyBottomBackgroundView: UIView!
}
