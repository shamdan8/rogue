import UIKit

class User2ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  required init?(coder aDecoder: NSCoder) {
    imageDictionary = [Int: UIImage]()
    productListings = [ProductListing]()
    super.init(coder: aDecoder)
  }
  
  @IBOutlet var tableView: UITableView!
  
  private var imageDictionary: [Int: UIImage]
  private var productListings: [ProductListing]

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    
    tableView.backgroundColor = UIColor(displayP3Red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
    
    ProductListingService.randomProductListings { output in
      do {
        let productListings = try output.value()

        DispatchQueue.main.async { [weak self] in
          self?.productListings = productListings
          self?.tableView.reloadData()
        }
      }
      catch {
        print(error)
      }
    }
  }
  
  func separatorInset(withCell cell: UITableViewCell) -> UIEdgeInsets {
    return UIEdgeInsets(
      top: 0,
      left: cell.separatorInset.left,
      bottom: 0,
      right: cell.separatorInset.left
    )
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    // should be an odd number so the background color matches first and last cell
    return productListings.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.row {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath)
      cell.selectionStyle = .none
      cell.textLabel?.font = UIFont.preferredFont(forTextStyle: .title2, withSymbolicTraits: .traitBold)
      cell.textLabel?.numberOfLines = 0
      cell.detailTextLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
      cell.detailTextLabel?.textColor = UIColor(displayP3Red: 89 / 255, green: 89 / 255, blue: 89 / 255, alpha: 1)
      cell.accessoryType = .none
      
      switch indexPath.section {
      case 0:
        cell.textLabel?.text = "Recently Played"
        cell.detailTextLabel?.text = "Audiobooks you listened to recently"
      case 1:
        cell.textLabel?.text = "Loans"
        cell.detailTextLabel?.text = "Audiobooks on loan"
      case 2:
        cell.textLabel?.text = "Holds"
        cell.detailTextLabel?.text = "Audiobooks on hold"
      case 3:
        cell.textLabel?.text = "Wishlist"
        cell.detailTextLabel?.text = "Audiobooks you'd like to read"
      case 4:
        cell.textLabel?.text = "Activity"
        cell.detailTextLabel?.text = "Your recent activity"
      default:
        fatalError()
      }
      
  
//      if indexPath.section % 2 == 0 {
//        cell.backgroundColor = UIColor(displayP3Red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
//      }
//      else {
//        cell.backgroundColor = UIColor(displayP3Red: 249 / 255, green: 249 / 255, blue: 249 / 255, alpha: 1)
//      }
      
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "body", for: indexPath) as! TitleTableViewCell
      cell.selectionStyle = .none
      
      let productListing = productListings[indexPath.section]
      
//      cell.titleTitleLabel.text = "Harry Potter and the Sorcerer's Stone"
//      cell.titleAuthorLabel.text = "J.K. Rowling"
      
      cell.productListingTitleLabel.text = productListing.title
      cell.productListingAuthorLabel.text = productListing.author
      
      cell.productListingTitleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline, withSymbolicTraits: .traitBold)
      cell.productListingAuthorLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
      cell.productListingAuthorLabel?.textColor = UIColor(displayP3Red: 89 / 255, green: 89 / 255, blue: 89 / 255, alpha: 1)
      
      cell.sectionBodyBottomBackgroundView.backgroundColor = UIColor(displayP3Red: 249 / 255, green: 249 / 255, blue: 249 / 255, alpha: 1)
      
//      cell.titleBottomBackgroundView.backgroundColor = UIColor(
//        displayP3Red: CGFloat.random(in: 0...1),
//        green: CGFloat.random(in: 0...1),
//        blue: CGFloat.random(in: 0...1),
//        alpha: CGFloat.random(in: 0...1)
//      )
      
      cell.productListingArtworkContainerView.layer.cornerRadius = 8
      
      cell.productListingBottomBackgroundView.backgroundColor = .white
      
      cell.productListingBottomBackgroundView.layer.cornerRadius = 8
      cell.productListingBottomBackgroundView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
      
      cell.productListingTopBackgroundView.backgroundColor = productListing.color
//      cell.titleTopBackgroundView.alpha = 0.05
      cell.productListingTopBackgroundView.layer.cornerRadius = 8
      cell.productListingTopBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
      
      cell.borderView.layer.cornerRadius = 8
      cell.borderView.layer.borderColor = UIColor.black.cgColor //UIColor(displayP3Red: 207 / 255, green: 207 / 255, blue: 207 / 255, alpha: 1).cgColor
      cell.borderView.layer.borderWidth = 0.25
      
//      if indexPath.section % 2 == 0 {
//        cell.backgroundColor = UIColor(displayP3Red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
//      }
//      else {
//        cell.backgroundColor = UIColor(displayP3Red: 249 / 255, green: 249 / 255, blue: 249 / 255, alpha: 1)
//      }
      
      if let image = imageDictionary[indexPath.section] {
//        print(cell.artworkImageView)
        cell.productListingArtworkImageView.image = image
      }
      else {
        cell.productListingArtworkImageView.image = nil
        
        if let url = productListing.artworkURL {
          let request = URLRequest(url: url)

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
              print(error)
            }
            else if let response = response as? HTTPURLResponse {
              if response.statusCode == 200 {
                if let data = data {
                  let image = UIImage(data: data)

                  DispatchQueue.main.async { [weak self] in
                    self?.imageDictionary[indexPath.section] = image
                    self?.tableView?.reloadRows(at: [indexPath], with: .none)
                  }
                }
              }
              else {

              }
            }
          }

          task.resume()
        }
      }
      
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "footer", for: indexPath)
      cell.selectionStyle = .none
      cell.textLabel?.text = nil
      cell.detailTextLabel?.font = UIFont.preferredFont(forTextStyle: .body)
      cell.detailTextLabel?.text = "See All"
      cell.detailTextLabel?.textColor = UIColor(displayP3Red: 123 / 255, green: 55 / 255, blue: 153 / 255, alpha: 1)
      cell.accessoryType = .disclosureIndicator
      
      cell.backgroundColor = UIColor(displayP3Red: 249 / 255, green: 249 / 255, blue: 249 / 255, alpha: 1)
      
//      if indexPath.section % 2 == 0 {
//        cell.backgroundColor = UIColor(displayP3Red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
//      }
//      else {
//        cell.backgroundColor = UIColor(displayP3Red: 249 / 255, green: 249 / 255, blue: 249 / 255, alpha: 1)
//      }
      
      return cell
    default:
      fatalError()
    }
  }
}
