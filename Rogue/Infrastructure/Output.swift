import Foundation

struct Output<Value> {
  init(_ error: Error) {
    wrapper = .failed(with: error)
  }
  
  init(_ value: Value) {
    wrapper = .succeeded(with: value)
  }
  
  private let wrapper: Wrapper<Value>
  
  func handle(with outputHandler: OutputHandler<Value>) {
    outputHandler(self)
  }
  
  func value() throws -> Value {
    return try wrapper.value()
  }
}

private enum Wrapper<Value> {
  case failed(with: Error)
  case succeeded(with: Value)
  
  func value() throws -> Value {
    switch self {
    case .failed(with: let error):
      throw error
    case .succeeded(with: let value):
      return value
    }
  }
}
