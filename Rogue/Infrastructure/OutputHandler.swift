import Foundation

typealias OutputHandler<Value> = (Output<Value>) -> Void
