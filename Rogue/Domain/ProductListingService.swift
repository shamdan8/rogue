import Foundation
import UIKit

struct ProductListingService {
  static func randomProductListings(outputHandler: @escaping OutputHandler<[ProductListing]>) {
    let productListings = [ProductListing]()
    randomProductListings(productListings: productListings, outputHandler: outputHandler)
  }
  
  private static func randomProductListings(productListings: [ProductListing], outputHandler: @escaping OutputHandler<[ProductListing]>) {
    randomProductListing { output in
      do {
        let productListing = try output.value()
        var productListings = productListings
        productListings.append(productListing)
        
        if productListings.count == 5 {
//          print(productListings)
          Output(productListings).handle(with: outputHandler)
        }
        else {
          randomProductListings(productListings: productListings, outputHandler: outputHandler)
        }
      }
      catch {
        randomProductListings(productListings: productListings, outputHandler: outputHandler)
//        Output(error).handle(with: outputHandler)
      }
    }
  }
  
  static func randomProductListing(outputHandler: @escaping OutputHandler<ProductListing>) {
    do {
      let id = Int.random(in: 0...4300000)
//      print(id)
      let string = "https://thunder-api.overdrive.com/v2/media/\(id)"
      
      guard let url = URL(string: string) else {
        throw ProductListingServiceError.requestURL(string)
      }
      
      let request = URLRequest(url: url)
      
      let task = URLSession.shared.dataTask(with: request) { data, response, error in
        do {
          if let error = error {
            throw ProductListingServiceError.dataTask(error)
          }
          else if let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
              if let data = data {
                let media = try JSONDecoder().decode(ThunderMedia.self, from: data)
                
                if let productListing = media.productListing() {
                  Output(productListing).handle(with: outputHandler)
                }
                else {
                  throw ProductListingServiceError.productListing(media)
                }
              }
            }
            else {
              throw ProductListingServiceError.responseStatusCode(url, response.statusCode)
            }
          }
        }
        catch {
          Output(error).handle(with: outputHandler)
        }
      }
      
      task.resume()
    }
    catch {
      Output(error).handle(with: outputHandler)
    }
  }
}

enum ProductListingServiceError: Error {
  case dataTask(Error)
  case jsonDecoder(Error)
  case productListing(ThunderMedia)
  case requestURL(String)
  case responseStatusCode(URL, Int)
}

extension ThunderMedia {
  func productListing() -> ProductListing? {
    guard let title = title else {
      return nil
    }
    
    var artworkURL: URL?
    
    if let string = covers?.cover150Wide?.href?.replacingOccurrences(of: "{", with: "%7B").replacingOccurrences(of: "}", with: "%7D") {
      if let url = URL(string: string) {
        artworkURL = url
      }
    }
    
    var color: UIColor?
    
    if let rgb = covers?.cover150Wide?.primaryColor?.rgb {
      if let red = rgb.red, let green = rgb.green, let blue = rgb.blue {
        color = UIColor(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: 1.0)
      }
    }
    
    return ProductListing(
      artworkURL: artworkURL,
      author: creators?.filter { $0.role == "Author" }.first?.name,
      color: color,
      title: title
    )
  }
}
