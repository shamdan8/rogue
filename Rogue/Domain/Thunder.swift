import Foundation

struct Thunder {

}

struct ThunderMedia: Codable {
  let bisacCodes: [String]?
  let covers: ThunderCoverList?
  let creators: [ThunderCreator]?
  let id: String?
  let formats: [ThunderFormat]?
  let fullDescription: String?
  let languages: [ThunderLanguage]?
  let isPublicDomain: Bool?
  let publisher: ThunderPublisher?
  let reserveId: String?
  let series: String?
  let subtitle: String?
  let sortTitle: String?
  let title: String?
  let type: ThunderMediaType?
}

struct ThunderCreator: Codable {
  let id: Int?
  let role: String?
  let name: String?
}

struct ThunderFormat: Codable {
  let id: String
}

struct ThunderLanguage: Codable {
  let id: String?
  let name: String?
}

struct ThunderPublisher: Codable {
  let id: String?
  let name: String?
}

struct ThunderMediaType: Codable {
  let id: String?
  let name: String?
}

struct ThunderCoverList: Codable {
  let cover150Wide: ThunderCover?
  let cover300Wide: ThunderCover?
  let cover510Wide: ThunderCover?
}

struct ThunderCover: Codable {
  let primaryColor: ThunderColor?
  let href: String?
}

struct ThunderColor: Codable {
  let rgb: ThunderRGB?
}

struct ThunderRGB: Codable {
  let blue: Int?
  let green: Int?
  let red: Int?
}
