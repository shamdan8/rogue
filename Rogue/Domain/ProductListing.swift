import Foundation
import UIKit

struct ProductListing {
  let artworkURL: URL?
  let author: String?
  let color: UIColor?
  let title: String
}
